package com.hrd.stockmanagementsystem.ui;
import com.hrd.stockmanagementsystem.model.Product;
import com.jakewharton.fliptables.FlipTable;

import java.util.List;
import java.util.Scanner;

import static com.hrd.stockmanagementsystem.model.Product.decreaseCounter;
import static com.hrd.stockmanagementsystem.services.Service.*;
import static com.hrd.stockmanagementsystem.utils.Util.*;

public class Ui {
    private Ui() {
    }
    public static final String[] header={"ID","NAME","UNIT PRICE","QUANTITY","IMPORTED DATE"};
    static int page = 1;
    static int chunk = 5;
    public static void welcome(){
        try {
            printWithThread("                                                      Welcome to\n",5,false);
            printWithThread("                                                   Stock Management\n",5,false);
            printWithThread("                       _____  _                   _____                       _____  _  _   \n",5,false);
            printWithThread("                      / ____|(_)                 |  __ \\                     / ____|| || | \n",5,false);
            printWithThread("                     | (___   _   ___  _ __ ___  | |__) | ___   __ _  _ __  | |  __ | || |_ \n",5,false);
            printWithThread("                      \\___ \\ | | / _ \\| '_ ` _ \\ |  _  / / _ \\ / _` || '_ \\ | | |_ ||__   _|\n",5,false);
            printWithThread("                      ____) || ||  __/| | | | | || | \\ \\|  __/| (_| || |_) || |__| |   | |  \n",5,false);
            printWithThread("                     |_____/ |_| \\___||_| |_| |_||_|  \\_\\\\___| \\__,_|| .__/  \\_____|   |_| \n",5,false);
            printWithThread("                                                                    | |                    \n",5,false);
            printWithThread("                                                                    |_|                    \n",5,false);
            System.out.println();
            printWithThread("Please Wait Loading...",100,false);
            System.out.println();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void displayMenu(List<Product> productList){
        Scanner input = new Scanner(System.in);
        System.out.println("╔════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗");
        System.out.println("║ *)Display   | W)rite  |   R)ead   |   U)pdate |   D)elete  |   F)irst  |  P)revious   |   N)ext   |   L)ast    ║");
        System.out.println("║ S)earch     | G)oto   |   Se)t row|   Sa)ve   |   Ba)ck up |   Re)store|  He)lp       |   E)xit                ║");
        System.out.println("╚════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝\n");
        System.out.print("Command -->");
        String inputChoice = input.next();
        String[] tempInput = inputChoice.split("(?<=\\d)(?=\\D)");
        try {
            Integer.valueOf(tempInput[0]);
            addProductByAmountUi(productList,tempInput);
        }catch (Exception e){
            choices(inputChoice.toLowerCase(),productList);
        }
    }
    public static void displayProduct(List<Product> productList){
        Scanner scanner = new Scanner(System.in);
        int id;
        Product product;
        String[][] data;
        boolean isFound = false;
        while(true){
            try{
                System.out.print("Read Product By ID: ");
                id = scanner.nextInt();
            }catch (Exception e){
                displayText("║                                               Invalid Input!                                                   ║");
                displayProduct(productList);
                break;
            }
            try {
                for (Product product1 : productList) {
                    if (product1.getId() == id) {
                        data = new String[][]{product1.getProductArray()};
                        System.out.println(FlipTable.of(header, data));
                        isFound = true;
                    }
                }
                if (!isFound){
                    displayText("║                                       There is no product by this ID                                           ║");
                    displayMenu(productList);
                    return;
                }
            }catch (Exception e){
                System.out.print("Y/y to try again or N/n to quit back to menu: ");
                if(scanner.next().equalsIgnoreCase("y")){
                    continue;
                }else {
                    break;
                }
            }
            break;
        }
        displayMenu(productList);
    }
    public static void choices (String choice,List<Product> productList) {
        switch (choice) {
            case "*":
                displayProductList(productList,"c",false,false);
                break;
            case "w":
                addNewProduct(productList);
                break;
            case "r":
                displayProduct(productList);
                break;
            case "u":
                updateExistProduct(productList);
                break;
            case "d":
                deleteProductUi(productList);
                break;
            case "f":
                displayProductList(productList,"f",false,false);
                break;
            case "p":
                displayProductList(productList,"p",false,false);
                break;
            case "n":
                displayProductList(productList,"n",false,false);
                break;
            case "l":
                displayProductList(productList,"l",false,false);
                break;
            case "s":
                searchByNameUi(productList);
                break;
            case "g":
                displayProductList(productList,"c",true,false);
                break;
            case "se":
                displayProductList(productList,"c",false,true);
                break;
            case "sa":
                saveToFile(productList);
                break;
            case "ba":
                backUpToDrive(productList);
                break;
            case "re":
                restoreFromBackup(productList);
                break;
            case "he":
                help(productList);
                break;
            case "e":
                break;
            default:
                defaultMenuAction(productList);
        }
    }
    private static void searchByNameUi(List<Product> productList){
        searchByName(productList);
        displayMenu(productList);
    }
    private static void saveToFile(List<Product> productList){
        save(productList);
        displayMenu(productList);
    }
    private static void addProductByAmountUi(List<Product> productList,String[] strings){
        if (!addProductByAmount(productList,strings)){
            displayText("║                                               Invalid Input!                                                   ║");
        }
        displayMenu(productList);
    }
    private static void backUpToDrive(List<Product> productList){
        backUp(productList);
        displayMenu(productList);
    }
    private static void restoreFromBackup(List<Product> productList){
        load(productList,true);
        displayMenu(productList);
    }
    public static void defaultMenuAction(List<Product> productList){
        displayText("║                                               Invalid Input!                                                   ║");
        displayMenu(productList);
    }
    public static void displayPages(int totalPage,int currentPage,int totalRecord){
        String text = "║  page: "+currentPage+" of "+totalPage+"                   Total Records : "+totalRecord+"  ║";
        displayText(text);
    }
    public static void displayText(String displayText){
        System.out.println(getTableLayout("╔","═","╗",displayText.length()));
        System.out.println(displayText);
        System.out.println(getTableLayout("╚","═","╝",displayText.length()));
        System.out.println("\n");
    }
    public static void displayProductList(List<Product> productList,String navigator,boolean jump,boolean setRow) {
        if (productList.isEmpty()){
            String text="║                                     There is no Product at the moment!                                         ║";
            displayText(text);
            displayMenu(productList);
            return;
        }
        int length;
        int totalPages;
        int row;
        int dataIndex = 0;
        int jumpTo;
        while (true){
            if (setRow){
                System.out.print("set row to :");
                try {
                    chunk = getInputInt();
                    if (chunk<0){
                        continue;
                    }
                }catch (Exception e){
                    displayText("║                                               Invalid Input!                                                   ║");
                    continue;
                }

                if (chunk==0){
                    System.out.println("Row must higher than 0");
                    continue;
                }
            }
            break;
        }
        Product product;
        String[][] data = new String[chunk][5];
        String[][] tempData;
        if (productList.size()%chunk!=0){
            totalPages = (productList.size()/chunk)+1;
        }else {
            totalPages = productList.size()/chunk;
        }
        while (true){
            if (jump){
                System.out.print("got to page: ");
                try{
                    jumpTo = getInputInt();
                }catch (Exception e){
                    System.out.println("Invalid Input !");
                    continue;
                }

                if (jumpTo>totalPages){
                    page = totalPages;
                    break;
                }else if(jumpTo<1){
                    page = 1;
                    break;
                } else {
                    page = jumpTo;
                }
            }break;
        }

        switch (navigator){
            case "n":
                page++;
                break;
            case "p":
                page--;
                break;
            case "f":
                page=1;
                break;
            case "l":
                page=totalPages;
                break;
            case "c":
                break;
            default:
                displayText("║                                               Invalid Input!                                                   ║");
        }
        if(page<1){page=totalPages;}
        if (page>totalPages){page=1;}
        row = (page*chunk)-chunk;
        if (page!=totalPages){
                length = page*chunk;
        }else {
                length = productList.size();
        }
        for (int i =row;i<length;i++) {
            product = productList.get(i);
            data[dataIndex] = product.getProductArray();
            dataIndex++;
        }
        if (dataIndex<chunk){
            tempData = new String[dataIndex][5];
            System.arraycopy(data, 0, tempData, 0, dataIndex);
            System.out.println(FlipTable.of(header,tempData));
            displayPages(totalPages,page,productList.size());
            displayMenu(productList);
            return;
        }
        System.out.println(FlipTable.of(header,data));
        displayPages(totalPages,page,productList.size());
        displayMenu(productList);
    }
    public static void updateExistProduct (List<Product> productList) {
        Scanner input = new Scanner(System.in);
        int id;
        int option;
        while (true){
            System.out.print("Please input Product ID : ");
            try {
                id = getInputInt();
                if (id<0){
                    if (id==-1){
                        displayMenu(productList);
                        return;
                    }
                    displayText("║                                               Negative Number                                                   ║");
                    continue;
                }
            } catch (Exception e) {
                displayText("║                                               Invalid Input!                                                   ║");
                continue;
            }
            break;
        }
        boolean isFound = false;
            for (Product product1 : productList) {
                if (product1.getId() == id) {
                    System.out.println(FlipTable.of(header, new String[][]{product1.getProductArray()}));
                    isFound = true;
                }
            }
            if (!isFound){
                displayText("║                                       There is no product by this ID                                           ║");
                displayMenu(productList);
                return;
            }

        System.out.println("What do you want to update?");
        displayText("║      1. All     2. Name     3. Quantity     4. Unit Price   5. Back to menu      ║");
        while (true){
            System.out.print("Option(1-5) : ");
            try {
                option = getInputInt();
                if (option<0 || option>5){
                    displayText("║                                               Invalid Input!                                                   ║");
                    continue;
                }
            } catch (Exception e) {
                displayText("║                                               Invalid Input!                                                   ║");
                continue;
            }
            break;
        }
        if (option==5){
            displayMenu(productList);
        }else {
            updateProduct(productList,option,id);
            displayMenu(productList);
        }
    }
    public static void addNewProduct (List<Product> productList) {
        Product product = addProduct(productList);
        System.out.println(FlipTable.of(header,new String[][]{
                product.getProductArray()})
        );
        System.out.print("Are you sure want to add this record? [Y/y] or [N/n] : ");
        if(confirmAnswer()) {
            productList.add(product);
            displayText("║                                " + product.getId() + " was added successfuly!                                      ║");
//            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//            System.out.println("        " + product.getId() + "  was added successfuly!");
//            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
        else {
            decreaseCounter();
            displayText("║                                Add product was cancelled!                                      ║");
        }
        displayMenu(productList);
    }
    public static void deleteProductUi(List<Product> productList){
        int id=0;
        while (true){
            System.out.print("Please input Product ID : ");
            try {
                id = getInputInt();
                if(id<0){
                    System.out.println("Id can not be lower than 0");
                }
            } catch (Exception e) {
                System.out.println("invalid input");
            }
            break;
        }
        boolean isFound = false;
        for (Product product1 : productList) {
            if (product1.getId() == id) {
                System.out.println(FlipTable.of(header, new String[][]{product1.getProductArray()}));
                isFound = true;
            }
        }
        if (!isFound){
            displayText("║                                       There is no product by this ID                                           ║");
            displayMenu(productList);
            return;
        }


        deleteProduct(productList,id);
        displayMenu(productList);
    }

//    method help
    public static void help (List<Product> productList) {
        displayText("║                                                  USAGE OF OUR SYSTEM                                                  ║");
        System.out.println(FlipTable.of(new String[] {"Symbol","Usage"},new String[][]{
                {"*",   "Display all product in list with pagination"},
                {"W/w", "Write or input new product into system"},
                {"R/r", "Read or output new product from the system"},
                {"U/u", "Update information to existing product"},
                {"D/d", "Delete a product from system"},
                {"F/f", "Go to the first page of all pages when displaying all products"},
                {"P/p", "Go to the previous page(page before the current page)"},
                {"N/n", "Go to the next page(page next to current page)"},
                {"L/l", "Go to the last page of all page"},
                {"S/s", "Search product by name"},
                {"G/g", "Go to the specific page(Input by user)"},
                {"Se",  "Set row of the table when displaying all products"},
                {"Sa",  "Save records to file"},
                {"Ba",  "Back up current state to new backup file"},
                {"Re",  "Restore record from backed up file"},
                {"He/he", "Help option for reading how to use the system"},
                {"E/e", "Exit from the system"},
                {"Number", "For inputting specific number of records. Ex: 1 000 = input 1000 records, 10 000 = 10 000 records"},
                {"suffix with \"K/k\"", "For inputting multiply value by 1 000 records. Ex: 1k = 1 000, 10k = 10 000"},
                {"suffix with \"M/m\"", "For inputting multiply value by 1 000 000 records. Ex: 1m = 1 000 000, 10m = 10 000 000"}})
        );
        displayMenu(productList);
    }
}

