package com.hrd.stockmanagementsystem.services;

import com.hrd.stockmanagementsystem.model.Product;
import com.jakewharton.fliptables.FlipTable;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import static com.hrd.stockmanagementsystem.ui.Ui.displayText;
import static com.hrd.stockmanagementsystem.utils.Util.*;

public class Service {
    public static void load(List<Product> productList, boolean restore){
        String folderName;
        if (restore){
            folderName = "backupfolder";
        }else {
            folderName = "save";
        }
        File file = new File(folderName);
        try {
            String[] tempStr;
            int id = 0;
            String name;
            String[] list = file.list();
            try {
                Arrays.sort(list);
            }catch (NullPointerException e){
                displayText("║                                             File Not Exist!                                                  ║");
                return;
            }

            String[][] data = new String[list.length][1];
            if (restore){
                for (int i = 0; i < list.length; i++) {
                    data[i] = new String[]{String.valueOf(i),list[i]};
                }
                System.out.println(FlipTable.of(new String[]{"ID","   BACKUP FILE LIST"}, data));
                while (true){
                    System.out.print("Enter Id to restore : ");
                    try {
                        id  = getInputInt();
                        if (id<0){
                            return;
                        }
                    } catch (Exception e) {
                        System.out.println("Invalid Input!");
                        continue;
                    }
                    break;
                }
                name = file.getAbsolutePath()+"/"+ list[id];
            }else {
                name = file.getAbsolutePath()+"/"+"save.txt";
            }
            productList.clear();
            long start = System.currentTimeMillis();

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(new FileInputStream(name), StandardCharsets.UTF_8))) {
                String line;
                while ((line = in.readLine()) != null) {
                    tempStr = line.split(",");
                    productList.add(new Product(Integer.parseInt(tempStr[0]),
                            tempStr[1],
                            Double.parseDouble(tempStr[2]),
                            Integer.parseInt(tempStr[3]),
                            LocalDate.parse(tempStr[4])));
                }
                Product.setCount(productList.size()+1);
            }
            long end = System.currentTimeMillis();
            System.out.println("loading... "+(end - start) / 1000f + " seconds");
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public static File backUp(List<Product> productList){
        File file = new File("backupfolder");
        boolean dirCreated = file.mkdir();
        if (dirCreated){
            System.out.println("backupfolder created");
        }
        String name =file.getAbsolutePath()+"/"+ LocalDateTime.now().withNano(0).toString()+".bak";
        try {
            FileWriter writer = new FileWriter(name);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            System.out.print("Writing to File ... ");
            writeToFile(productList, bufferedWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
    public static void save(List<Product> productList){
        File file = new File("save");
        boolean dirCreated = file.mkdir();
        if (dirCreated){
            System.out.println("save folder created");
        }
        String name =file.getAbsolutePath()+"/"+"save.txt";
        new File(name).delete();
        try {
            FileWriter writer = new FileWriter(name);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            System.out.print("Writing to file ... ");
            writeToFile(productList, bufferedWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static boolean addProductByAmount(List<Product> productList,String[] strings){
        int amount = 0;
        int modifier =1;

        if (!(strings.length<2)){

            switch (strings[1].toLowerCase()){
                case "m":
                    modifier*=1000000;
                    break;
                case "k":
                    modifier*=1000;
                    break;
                default:
                    return false;
            }
        }
        try {
            if (Integer.parseInt(strings[0])<0){
                return false;
            }
            amount = Integer.parseInt(strings[0])*modifier;  
        }catch (Exception e){
            System.out.println("error here!");
        }
        
        long start = System.currentTimeMillis();
        for (int i = 0; i < amount; i++) {
            productList.add(new Product("Name",12,2));
        }
        long end = System.currentTimeMillis();
        System.out.println("add "+amount+" file to list : "+(end - start) / 1000f + " seconds");
        return true;
    }
    public static boolean updateProduct(List<Product> productList,int option,int id){
        Scanner scanner = new Scanner(System.in);
        Product product2 = null;
        boolean isFound = false;
        for (Product product1 : productList) {
            if (product1.getId()==id){
                product2 = product1;
                isFound = true;
            }
        }
        Product product;
        if (option==1){
            product = setProductInput(true,true,true,id);
            System.out.print("Are you sure want to update this record? [Y/y] or [N/n] : ");
            if (scanner.next().equalsIgnoreCase("y")){
                displayText("║                                   Product update successfully                                   ║");
                product2.setProduct(product.getName(),product.getUnitPrice(),product.getQty());
                return true;
            }else {
                Product.decreaseCounter();
                displayText("║                                    Update Cancel                                    ║");
                return false;
            }

        }
        if (option==2){
            product = setProductInput(true,false,false,id);
            System.out.print("Are you sure want to update this record? [Y/y] or [N/n] : ");
            if (scanner.next().equalsIgnoreCase("y")){
                displayText("Product update successfully");
                product2.setName(product.getName());
                return true;
            }else {
                Product.decreaseCounter();
                displayText("║                                    Update Cancel                                    ║");
                return false;
            }
        }
        if (option==3){
            product = setProductInput(false,false,true,id);
            System.out.print("Are you sure want to update this record? [Y/y] or [N/n] : ");
            if (scanner.next().equalsIgnoreCase("y")){
                displayText("║                                   Product update successfully                                   ║");
                product2.setQty(product.getQty());
                return true;
            }else {
                Product.decreaseCounter();
                displayText("║                                    Update Cancel                                    ║");
                return false;
            }
        }
        if (option==4){
            product = setProductInput(false,true,false,id);
            System.out.print("Are you sure want to update this record? [Y/y] or [N/n] : ");
            if (scanner.next().equalsIgnoreCase("y")){
                displayText("║                                    Product update successfully                                    ║");
                product2.setUnitPrice(product.getUnitPrice());
                return true;
            }else {
                Product.decreaseCounter();
                displayText("║                                    Update Cancel                                    ║");
                return false;
            }
        }
        return false;
    }
    public static Product setProductInput(boolean name,boolean unitPrice,boolean qty,int id){
        Scanner scanner = new Scanner(System.in);
        Product product = new Product("abc",12,12);
        String _name;
        double _unitsPrice;
        int _qty;
        if (name){
            System.out.print("Enter product name : ");
            _name = scanner.nextLine();
            product.setName(_name);
        }
        if (unitPrice){
            while(true){
                try {
                    System.out.print("Enter product unit price : ");
                    _unitsPrice = getInputDouble();
                    if (_unitsPrice<0){
                        displayText("║                                                Negative Number!                                                   ║");
                        continue;
                    }
                } catch (Exception e) {
                    displayText("║                                               Invalid Input!                                                   ║");
                    continue;
                }
                product.setUnitPrice(_unitsPrice);
                break;
            }
        }
        if (qty){
            while(true){
                try {
                    System.out.print("Enter quantity : ");
                    _qty = getInputInt();
                    if (_qty<0){
                        displayText("║                                                Negative Number!                                                   ║");
                        continue;
                    }
                } catch (Exception e) {
                    displayText("║                                               Invalid Input!                                                   ║");
                    continue;
                }
                product.setQty(_qty);
                break;
            }
        }
        return product;
    }
    public static Product addProduct(List<Product> productList) {
        Scanner input = new Scanner(System.in);
        int qty = 0;
        double unitPrice = 0;
        Product product = new Product("a", 1, 1);
        System.out.println("Product ID : " + product.getId());
        System.out.print("Product's name : ");
        String name = input.nextLine();
        while (true){
            System.out.print("Product's price : ");
            try {
                unitPrice = getInputDouble();
                if (unitPrice<0){
                    displayText("║                                               Invalid Input!                                                   ║");
                    continue;
                }
            } catch (Exception e) {
                displayText("║                                               Invalid Input!                                                   ║");
                continue;
            }
            break;
        }
        while (true){
            System.out.print("Product's quantity : ");
            try {
                qty = getInputInt();
                if (qty<0){
                    displayText("║                                               Invalid Input!                                                   ║");
                    continue;
                }
            } catch (Exception e) {
                displayText("║                                               Invalid Input!                                                   ║");
                continue;
            }
            break;
        }

        product.setProduct(name, unitPrice, qty);
        return product;
    }
    public static boolean deleteProduct (List<Product> productList, int id) {
        Scanner scanner = new Scanner(System.in);
        Product product2 = null;
        System.out.print("Are you sure want to delete this record?[Y/y] or [N/n]:");
        if(scanner.next().equalsIgnoreCase("Y")){
            for (Product product1 : productList) {
                if (product1.getId()==id){
                    product2 = product1;
                }
            }
            productList.remove(product2);
            displayText("║                                    Product removed                                                   ║");
            return true;
        }else{
            displayText("║                                    delete canceled                                                   ║");
            return false;
        }
    }
    public static void searchByName(List<Product> productList){
        Scanner sc = new Scanner(System.in);
        String name = null;
        System.out.print("Enter name to search: ");
        name = sc.nextLine();
        boolean isFound = false;
        List<String[]> dataList = new ArrayList<>();
        for(Product product : productList){
            if (Pattern.compile(name,Pattern.CASE_INSENSITIVE).matcher(product.getName()).find()){
                dataList.add(product.getProductArray());
                isFound = true;
            }
        }
        String[][] data = new String[dataList.size()][5];
        for (int j = 0; j < dataList.size(); j++) {
            data[j] = dataList.get(j);
        }
        if (!isFound){
            displayText("║                                       Search not found                                 ║");
        }else {
            System.out.println(FlipTable.of(new String[]{"ID","NAME","UNIT PRICE","QUANTITY","IMPORTED DATE"},data));
        }
    }
}
