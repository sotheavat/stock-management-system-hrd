   **What we have done in this project are:**

1. Display all products.
2. Write data to the system.
3. Read data by ID provided by user.
4. Update product information.
5. Delete product from the system.
6. Pagination when have a lot of products to show such as:
    -   First page
    -   Previous page
    -   Next page
    -   Last page
7. Search product by name provided by user.
8. Go to specific page input by user
9. Set amount of table's row in each pages
10. Save records to file
11. Back up data to a new file
12. Restore file from back up.
13. Help user to know how to use the system such as usage of each commands in menu.  
14. Use shortcut to manipulate data such as: 1k = 1000 records, 10m = 10 000 000 records.
